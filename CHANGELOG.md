This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Test WebApp

## [v1.0.0-SNAPSHOT] - 2022-05-11

- First Release

