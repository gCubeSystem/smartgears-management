FROM luciolelii/smartgears:4.0.0-SNAPSHOT
  COPY logback.xml /etc/
  COPY container.ini /etc/
  COPY target/test-app.war /tomcat/webapps 