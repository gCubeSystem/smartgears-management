package org.gcube.test.app;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.smartgears.ApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAppManager implements ApplicationManager {

	Logger logger = LoggerFactory.getLogger(MyAppManager.class);

	private boolean init = false;

	@Override
	public void onInit() {
		try {
			init = true;
			logger.info("init called in {}: {}",SecretManagerProvider.instance.get().getContext(), this.init);	
		}catch (Exception e) {
			logger.error("error on init ",e);
		}
	}

	@Override
	public void onShutdown() {
		logger.info("shutdown called");		
		logger.info("shutdown called in {}: {}",SecretManagerProvider.instance.get().getContext(), this.init);	
	}

	public boolean isInit() {
		return init;
	}	

	
}