package org.gcube.test.app;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.ApplicationManagerProvider;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(urlPatterns = "/*", name = "testAppServlet")
@ManagedBy(MyAppManager.class)
public class TestAppServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	// log on behalf of extension
	private static final Logger log = LoggerFactory.getLogger(TestAppServlet.class);
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
		log.info("testApp call received");
		Secret secret = SecretManagerProvider.instance.get();
		String context = secret !=null? secret.getContext():null;
		log.info("secret si {} and context {}",secret, context);
		
		MyAppManager appManager = (MyAppManager)ApplicationManagerProvider.get(MyAppManager.class);
		
		response.getWriter().write("test app started with initialization "+appManager.isInit() );
	}

}